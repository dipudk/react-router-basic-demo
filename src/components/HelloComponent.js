import React from 'react';
const Hello = () => {  // ES6 form of function Hello() { }
    return (
        <div className="jumbotron">
            <h1 className="display-3">Hello, world!</h1>
        </div>
    );
}
export default Hello;
