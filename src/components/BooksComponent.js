import React from 'react';
import {Link,Route} from'react-router-dom';

const Books = ({ match }) => {
    return (
        <div>
            <div className="jumbotron">
                <h1 className="display-3">My Books</h1>
            </div>
            <div className="container">
                <div className="row">
                    <div className="col-md-3">
                        <ul>
                        {/* ${match.url} evaluates to /books and ${match.path} evaluates to localhost://3000/books. */}
                            <li><Link to={`${match.url}/html`} >HTML</Link></li>
                            <li><Link to={`${match.url}/css`}>CSS</Link></li>
                            <li><Link to={`${match.url}/react`}>React</Link></li>
                        </ul>
                    </div>
                    <div className="col-md-9">
                    {/* place routes here */}
                        <Route path={`${match.path}/html`} render={() => <h1>Learn HTML by ABC</h1> } />
                        <Route path={`${match.path}/css`} render={() => <h1>Learn CSS by DEF</h1> } />
                        <Route path={`${match.path}/react`} render={() => <h1>Learn React by GHI</h1> } />
                        <Route path={`${match.path}/:id`} component={User} />
                    </div>
                </div>
            </div>
        </div>
    );
}

const User = ({ match }) => {
    return (
        <h1>Welcome, {match.params.id}</h1>
    );
}

export default Books;
