import React, { Component } from 'react';
import './App.css';

// import components here
import Hello from './components/HelloComponent';
import About from './components/AboutComponent';
import Books from './components/BooksComponent';

// import route Components here
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom';

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <div className="container">
            <ul>
              <li><Link to="/hello">Hello</Link></li>
              <li><Link to="/about">About</Link></li>
              <li><Link to="/books">Books</Link></li>
              <li><Link to="/hello/goodmorning">Good Morning</Link></li>
            </ul>
            <hr/>
          {/* Routes will go here */}
          <Switch>
              
              <Route path={'/hello/goodmorning'} render={() =>{
                return (
                  <div className="jumbotron">
                    <h1 className="display-3">Hello, Good Morning</h1>
                  </div>
                );
              }} />
              <Route path="/hello" component={Hello} />
              <Route path="/about" component={About} />
              <Route path="/books" component={Books} />
              
              <Route path={'/'} render={() => {
              return (
                <div className="jumbotron">
                  <h1 className="display-3">Homepage</h1>
                </div>
              ); 
            }}/>
            </Switch>
          </div>
        </div>
      </Router>
    );
  }
}
export default App;
